# S&box unofficial documentation
View the documentation at https://rtm516.gitlab.io/sbox_docs/

If you wish to contribute to this project just make some changes in the c# code under src, comment those changes and then create a pull request.