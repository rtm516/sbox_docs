# Welcome to the S&box unofficial documentation
We aim to document the functions and classes of [S&box](https://sandbox.facepunch.com/) an upcoming game by [Facepunch Studios](https://facepunch.com/). If you wish to contribute to this project just make some changes in the C# code under src, comment those changes and then create a pull request.

**This is not complete and more functions and classes need to be documented based on the below list of urls**

~~[Official S&box Discord](https://discordapp.com/invite/juEJDns)~~


The infomation in this documentation was pulled from:
* https://github.com/Facepunch/sbox-suicidebarrels/ - Example gamemode
* https://gist.github.com/aylaylay/8583469f23a1717beb7329b6cb889186 - Looks like a direct api class
* https://gist.github.com/aylaylay/bb4a018d17d4af2ce8cddd3e9b505329 - Looks like a direct api class
* https://gist.github.com/aylaylay/dc7f13393209684b3305f386a404972b - Leaf Blower tool
* https://gist.github.com/aylaylay/1103b3f982e78ff89e4f91c38d3722de - Some form of npc?
* https://gist.github.com/aylaylay/2bb2546b39414ef4b187bd6d6783c71e - Some friend HUD element?
* https://gist.github.com/aylaylay/5683fb206369c3cf8fc841ca995d68c7 - An emitter?
* https://gist.github.com/aylaylay/6d8b7107d1ddc9cee0c30da9a3f844e3 - Another emitter?
* https://gist.github.com/aylaylay/ef39ada13b22962248ad109761148cc1 - Obj model importer (Possibly from the game code)
* https://gist.github.com/aylaylay/e1a099e76539895620222f8d3d4cc7cd - Balloon entity
* https://gist.github.com/aylaylay/c4cad1fac95e5c6bc15a4b71ef9a1b1e - An outside scene entity?
* https://gist.github.com/aylaylay/864f7c3c2e92541960cb8adb540a3815 - Thruster tool
* https://gist.github.com/aylaylay/d9c1a2bb2b609dd493bd62c98923ff0b - Hitscan tool
* https://gist.github.com/aylaylay/2d302f566b8f260c97748509eb0e592f - Looks like a direct api class
* https://gist.github.com/aylaylay/be9cfe432b2b8ecf6a193bf01055db22 - Explosion - Looks like a direct api class
* https://gist.github.com/aylaylay/638eebaeac43d7df1cc14788cc210dfb - Looks like a direct api class (Probably inherits from controllable)
* https://gist.github.com/aylaylay/884564bd941cf4a8a908e6f2563209e5 - Tower Unite model parser V1
* https://gist.github.com/aylaylay/903fec1a44c9826bd17e9cfe872bcc6e - Tower Unite model parser V2
* https://crayz.tv/sbox-gameplay-screenshots/ - Unofficial collection of screenshots and infomation (has code screenshots)
