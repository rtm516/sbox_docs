﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sandbox.Import
{
    [ClassLibrary]
    public class ObjModel : Importer<Model>
    {
        public override bool CanLoadExtension(string extension)
        {
            if (extension == ".obj") return true;

            return false;
        }

        public override void Reload(Model model)
        {
            if (!string.IsNullOrEmpty(model.Path))
            {
                Load(model, model.Path);
            }
        }

        public override Model Load(string absoluteName)
        {
            var model = new Model { IsLoading = true };

            Load(model, absoluteName);

            model.IsLoading = false;

            return model;
        }

        private struct ObjFace
        {
            public int MaterialIndex;
            public int IndexCount;
            public ObjVertexIndex[] Indices;
        }

        private struct ObjVertexIndex
        {
            public int VertexIndex;
            public int TextureIndex;
            public int NormalIndex;
        }

        void Load(Model model, string filename)
        {
            using (model.LoadTime.Record("Load"))
            {
                var v = new List<Vector3>();
                var vt = new List<Vector2>();
                var vn = new List<Vector3>();
                var f = new List<ObjFace>();
                var m = new List<string>() { "default" };
                var mtl = string.Empty;

                var lines = FileSystem.ReadAllText(filename).Split(Environment.NewLine.ToCharArray());
                foreach (var line in lines)
                {
                    var parts = line.Split(' ', StringSplitOptions.RemoveEmptyEntries);
                    if (parts.Length == 0) continue;

                    switch (parts[0])
                    {
                        case "v":
                            v.Add(new Vector3(parts[1].ToFloat(), parts[2].ToFloat(), parts[3].ToFloat()));
                            break;
                        case "vt":
                            vt.Add(new Vector2(parts[1].ToFloat(), parts[2].ToFloat()));
                            break;
                        case "vn":
                            vn.Add(new Vector3(parts[1].ToFloat(), parts[2].ToFloat(), parts[3].ToFloat()));
                            break;
                        case "usemtl":
                            mtl = parts[1];
                            m.Add(mtl);
                            break;
                        case "f":
                            var sides = parts.Length - 1;
                            var face = new ObjFace
                            {
                                IndexCount = sides,
                                MaterialIndex = m.Count - 1,
                                Indices = new ObjVertexIndex[sides]
                            };
                            for (var i = 0; i < sides; ++i)
                            {
                                var subparts = parts[i + 1].Split('/');
                                face.Indices[i] = new ObjVertexIndex
                                {
                                    VertexIndex = subparts[0].ToInt(),
                                    TextureIndex = subparts[1].ToInt(),
                                    NormalIndex = subparts[2].ToInt(),
                                };
                            }
                            f.Add(face);
                            break;
                    }
                }

                var faceGroups = f.GroupBy(x => x.MaterialIndex);
                var hasNormals = vn.Count != 0;
                var hasTextureCoords = vt.Count != 0;

                foreach (var faceGroup in faceGroups)
                {
                    var mesh = new Mesh
                    {
                        Name = m[faceGroup.Key],
                        CastShadow = true,
                        Collisions = true,
                    };

                    var vertexLookup = new Dictionary<ObjVertexIndex, int>();

                    foreach (var face in faceGroup)
                    {
                        for (var j = 0; j < face.IndexCount; ++j)
                        {
                            var faceIndex = face.Indices[j];
                            if (!vertexLookup.ContainsKey(faceIndex))
                            {
                                vertexLookup.Add(faceIndex, mesh.Vertex.Count);
                                mesh.Vertex.Add(new Vertex
                                {
                                    Position = v[faceIndex.VertexIndex - 1],
                                    UV1 = hasTextureCoords ? vt[faceIndex.TextureIndex - 1] : Vector2.Zero,
                                    Normal = hasNormals ? vn[faceIndex.NormalIndex - 1] : Vector3.Zero,
                                    Color = Color.White,
                                });
                            }
                        }

                        if (face.IndexCount == 3)
                        {
                            var i0 = vertexLookup[face.Indices[0]];
                            var i1 = vertexLookup[face.Indices[1]];
                            var i2 = vertexLookup[face.Indices[2]];

                            mesh.Indices.Add((uint)i2);
                            mesh.Indices.Add((uint)i1);
                            mesh.Indices.Add((uint)i0);
                        }
                        else if (face.IndexCount == 4)
                        {
                            var i0 = vertexLookup[face.Indices[0]];
                            var i1 = vertexLookup[face.Indices[1]];
                            var i2 = vertexLookup[face.Indices[2]];
                            var i3 = vertexLookup[face.Indices[3]];

                            mesh.Indices.Add((uint)i2);
                            mesh.Indices.Add((uint)i1);
                            mesh.Indices.Add((uint)i0);

                            mesh.Indices.Add((uint)i0);
                            mesh.Indices.Add((uint)i3);
                            mesh.Indices.Add((uint)i2);
                        }
                    }

                    model.Meshes.Add(mesh);
                }

                if (model.Meshes.Count > 0)
                {
                    using (model.LoadTime.Record("Transforming"))
                    {
                        var settings = Model.Library.ImportSettings;
                        model.ApplyTransform(settings.Rotation, settings.Scale, settings.Origin);
                    }

                    using (model.LoadTime.Record("Commit"))
                    {
                        model.Commit();
                    }

                    using (model.LoadTime.Record("Init Physics"))
                    {
                        model.InitPhysics();
                    }

                    var importSettings = Model.Library.ImportSettings;

                    model.CollisionMode = importSettings.Collision;

                    if (model.CollisionMode == CollisionMode.Simple ||
                        model.CollisionMode == CollisionMode.Mixed)
                    {
                        using (model.LoadTime.Record("Convex Physics"))
                        {
                            model.CreateConvexPhysics(importSettings.LodForCollision);
                        }
                    }

                    using (model.LoadTime.Record("Commit Physics"))
                    {
                        model.CommitPhysics();
                    }
                }
            }
        }
    }
}