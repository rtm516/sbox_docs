﻿namespace Sandbox
{
    public class BaseAddon
    {
        /// <summary>
        /// Called when the addon is loaded
        /// </summary>
        /// <param name="resources"></param>
        public void Loaded(IAddonResources resources) { }
    }
}
