﻿namespace Sandbox
{
    public class Controllable : BaseEntity
    {
        /// <summary>
        /// The eye angles of the controllable entity
        /// </summary>
        public Quaternion EyeAngles;

        /// <summary>
        /// The location on the client? of the entity
        /// </summary>
        public Vector3 ClientLocation;

        /// <summary>
        /// The eye angles on the client? of the entity
        /// </summary>
        public Quaternion ClientEyeAngles;

        /// <summary>
        /// The player this controllable is linked to
        /// </summary>
        public Player Player;

        /// <summary>
        /// Moves the controllable to the specified position
        /// </summary>
        /// <param name="position">The position to move then entity to</param>
        public void Teleport(Vector3 position) { }
        
        /// <summary>
        /// Called when the controllable has been respawned
        /// </summary>
        public void OnRespawned() { }

        protected virtual void DoInput() { }
        protected virtual void TickMovement() { }
        public virtual void OnUpdateView(ref ViewInfo viewInfo) { }
    }
}
