﻿namespace Sandbox
{
    public class Color
    {
        /// <summary>
        /// Static red color
        /// </summary>
        public static Color Red;
        
        /// <summary>
        /// Static green color
        /// </summary>
        public static Color Green;

        /// <summary>
        /// Static white color
        /// </summary>
        public static Color White;

        /// <summary>
        /// The red value of the color
        /// </summary>
        public double r;

        /// <summary>
        /// The green value of the color
        /// </summary>
        public double g;

        /// <summary>
        /// The blue value of the color
        /// </summary>
        public double b;

        /// <summary>
        /// Creates a new color with the specified values
        /// </summary>
        /// <param name="r">The new colors red value</param>
        /// <param name="g">The new colors green value</param>
        /// <param name="b">The new colors blue value</param>
        public Color(double r, double g, double b) { }
    }
}
