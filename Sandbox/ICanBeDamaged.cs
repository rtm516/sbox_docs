﻿namespace Sandbox
{
	public interface ICanBeDamaged
	{
		public bool TakeDamage(DamageInfo damage);
	}
}