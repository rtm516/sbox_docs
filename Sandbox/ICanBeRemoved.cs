﻿namespace Sandbox
{
	public interface ICanBeRemoved
	{
		public bool Remove();
	}
}