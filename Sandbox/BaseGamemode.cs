﻿using System;

namespace Sandbox
{
    public class BaseGamemode
    {
        /// <summary>
        /// If we are the server
        /// </summary>
        public bool Server;

        /// <summary>
        /// If we are the Client
        /// </summary>
        public bool Client;

        /// <summary>
        /// Unsure seems to act like Server
        /// </summary>
        public bool Authority;

        /// <summary>
        /// Called when the gamemode is created
        /// </summary>
        protected virtual void Initialize() { }

        /// <summary>
        /// Called every tick
        /// </summary>
        protected virtual void Tick() { }

        /// <summary>
        /// Creates a controllable (movement and view?) for the player
        /// </summary>
        /// <param name="player">The player to create the controllable for</param>
        /// <returns>An instance of controllable for the player</returns>
        public virtual Controllable CreateControllable(Player player) { return default(Controllable); }

        /// <summary>
        /// Loads in the specified map
        /// </summary>
        /// <param name="mapName">The map to load</param>
        public virtual void LoadMap(string mapName) { }

        /// <summary>
        /// Called when a player joins
        /// </summary>
        /// <param name="player">The player that joined</param>
        public virtual void OnPlayerJoined(Player player) { }

        /// <summary>
        /// Called when a player left
        /// </summary>
        /// <param name="player">The player that left</param>
        public virtual void OnPlayerLeave(Player player) { }

        /// <summary>
        /// Called when a player dies
        /// </summary>
        /// <param name="player">The player that died</param>
        public virtual void OnPlayerDied(Player player) { }

        /// <summary>
        /// Called when a player dies
        /// </summary>
        /// <param name="player">The player that died</param>
        /// <param name="killer">The killer</param>
        public virtual void OnPlayerDied(Player player, Controllable killer) { }

        /// <summary>
        /// Called when a player adds a message to chat
        /// </summary>
        /// <param name="playerName">The players name</param>
        /// <param name="team">The players team</param>
        /// <param name="message">The message that was added to chat</param>
        public virtual void OnPlayerMessage(string playerName, int team, string message) { }

        /// <summary>
        /// Called when the player wishes to add a message to chat
        /// </summary>
        /// <param name="sender">The player sending the message</param>
        /// <param name="receiver">Another player?</param>
        /// <param name="message">The message sent</param>
        /// <returns></returns>
        public virtual bool AllowPlayerMessage(Player sender, Player receiver, string message) { return false; }

        /// <summary>
        /// Called when the player should be respawned
        /// </summary>
        /// <param name="player">The player to respawn</param>
        public virtual void RespawnPlayer(Player player) { }

        /// <summary>
        /// Called when some form of input is detected
        /// </summary>
        public virtual void OnLocalInput() { }
    }
}
