﻿namespace Sandbox
{
    [DataContract]
    public class ParticleEmitter
    {
        public ParticleEmitter(ParticleSystem system);

        //
        // Summary:
        //     If true then collisions will apply physics to objects they hit (particle won't
        //     be affected)
        public bool CollisionApplyPhysics;
        //
        // Summary:
        //     If true then collisions will apply physics to objects they hit (particle won't
        //     be affected)
        public bool CollisionIgnoreTriggers;
        public Curve3 CollisionDamp;
        public bool EnableCollisions;
        //
        // Summary:
        //     If you're further away from this the particles won't bother colliding. This defaults
        //     to 5000, which is 50 meters.
        public double CollisionDistance;
        public int CollisionMax;
        public ParticleCollisionComplete OnCollisionMax;
        public Curve CollisionMass;
        public Curve Drag;
        public Curve DragOverLife;
        public Curve3 Acceleration;
        public Curve3 AccelerationOverLife;
        public Curve RotationRate;
        public Curve3 LightColorOverLife;
        public bool LightHighQuality;
        public double LightSpawnFraction;
        public Curve LightBrightnessOverLife;
        public Curve LightRadius;
        public Curve LightExponent;
        public ParticleAlignment Align;
        public ParticleSort Sort;
        public Model Model;
        public bool ModelCastShadows;
        public string SpawnColorParameter;
        public string BurstScaleParameter;
        public string StartPositionParameter;
        public bool LightCastsShadows;
        public Curve BurstScale;
        public Curve StartRotation;
        public Curve SpawnRadius;
        public bool LocalSpace;
        public int MaxDrawCount;
        public int LoopCount;
        public Vector3 Origin;
        public Material Material;
        public Curve3 StartPosition;
        public Curve AlphaOverLife;
        public Curve AlphaScaleOverLife;
        public Curve3 ColorOverLife;
        public Curve SpawnRate;
        public Curve LifeTime;
        public Curve3 ColorScaleOverLife;
        public Vector3 SpeedScale;
        public Curve3 LifeScale;
        public Curve3 SpawnColor;
        public Vector3 SpeedScaleMax;
        public Curve SpawnRadialVelocity;
        public Curve3 SpawnVelocity;
        public Curve3 SpawnSize;
        public Curve Alpha;
        public bool OverrideModelMaterial;

        public ParticleEmitter AddCollisionEvent(int frequency = 0, bool firstTimeOnly = false, bool lastTimeOnly = false) { }
        public ParticleEmitter WithBurst(int minCount, int maxCount, float delay = 0) { }
        public ParticleEmitter WithBurst(int count, float delay = 0) { }
        public ParticleEmitter WithCollide(params CollisionChannel[] channels) { }
        public ParticleEmitter WithConeVelocity(Curve angle, Curve velocity) { }
        public ParticleEmitter WithConeVelocity(Curve angle, Curve velocity, Vector3 direction) { }
        public ParticleEmitter WithGravity() { }
    }
}