﻿namespace Sandbox
{
    public static class Log
    {
        /// <summary>
        /// Creates an 'Info' log in console and if obj is string then args are passed into it
        /// </summary>
        /// <param name="obj">The item to be turned into a string and logged</param>
        /// <param name="args">The items to insert into the string</param>
        public static void Info(object obj, params object[] args) { }

        /// <summary>
        /// Creates an 'Assert' log in console and if obj is string then args are passed into it
        /// </summary>
        /// <param name="obj">The item to be turned into a string and logged</param>
        /// <param name="args">The items to insert into the string</param>
        public static void Assert(object obj, params object[] args) { }

        /// <summary>
        /// Creates an 'Warning' log in console and if obj is string then args are passed into it
        /// </summary>
        /// <param name="obj">The item to be turned into a string and logged</param>
        /// <param name="args">The items to insert into the string</param>
        public static void Warning(object obj, params object[] args) { }
    }
}
