﻿namespace Sandbox
{
    [ClassLibrary("DefaultControllable")]
    public class DefaultControllable : Controllable
    {
        protected override void DoInput()
        {
            base.DoInput();

            var movementInput = Vector3.Zero;

            if (Input.IsDown(Button.W)) movementInput += Vector3.Forward;
            if (Input.IsDown(Button.S)) movementInput -= Vector3.Forward;
            if (Input.IsDown(Button.D)) movementInput += Vector3.Right;
            if (Input.IsDown(Button.A)) movementInput -= Vector3.Right;

            movementInput = movementInput.Normal;

            if (Input.IsDown(Button.LeftShift)) movementInput *= 10.0;
            else if (Input.IsDown(Button.LeftControl)) movementInput *= 0.5;

            double yaw = Input.Value(Axis.MouseX) * 0.05;
            double pitch = Input.Value(Axis.MouseY) * -0.05;

            AddMovementInput(movementInput);
            AddEyeInput(pitch, yaw);
        }

        protected override void TickMovement()
        {
            base.TickMovement();

            var inputVector = ConsumeInputVector();
            var moveDirection = ControlRotation.Forward * inputVector.x + ControlRotation.Right * inputVector.y;

            var velocity = moveDirection * 1000.0;
            Position += velocity * Time.Delta;
        }

        public override void OnUpdateView(ref ViewInfo viewInfo)
        {
            viewInfo.Fov = 90.0;
        }
    }
}