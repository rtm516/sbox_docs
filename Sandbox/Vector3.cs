﻿namespace Sandbox
{
    public class Vector3
    {
        /// <summary>
        /// The up vector
        /// </summary>
        public static Vector3 Up;

        /// <summary>
        /// An empty vector
        /// </summary>
        public static Vector3 Zero;
    }
}
