﻿namespace Sandbox
{
    [ClassLibrary]
    public class OutsideScene : BaseEntity
    {
        protected override void Initialize()
        {
            base.Initialize();

            if (Authority)
            {
                Replicates = true;
                ReplicateMovement = false;
                Moveable = false;
            }
        }

        protected override void OnAuthoritySpawn()
        {
            base.OnAuthoritySpawn();

            OnSpawn();
        }

        protected override void OnRemoteSpawn()
        {
            base.OnRemoteSpawn();

            OnSpawn();
        }

        protected virtual void OnSpawn()
        {
            if (Authority)
            {
                new PlayerStartEntity
                {
                    Position = Vector3.Up * 100.0,
                }
                .Spawn();
            }

            SetupEnvironment();
        }

        private void SetupEnvironment()
        {
            new DirectionalLight
            {
                LightColor = Color.White,
                LightIntensity = 2.5,
                CastShadows = true,
                Rotation = Quaternion.FromAngles(-45, 45, 0),
                Replicates = false,
                ReplicateMovement = false,
                TickEnabled = false,
                Moveable = true,
            }
            .Spawn();

            new SkyLight
            {
                LightColor = Color.White,
                LightBrightness = 0.5,
                LightLowerHemisphere = true,
                CubemapTextureName = "Materials/cubemap.png",
                Replicates = false,
                ReplicateMovement = false,
                TickEnabled = false,
                Moveable = false,
            }
            .Spawn();

            var skySphere = new BaseModelEntity
            {
                CastShadow = false,
                LightingChannels = LightingChannel.None,
                Scale = 100.0,
                Collision = false,
                Replicates = false,
                ReplicateMovement = false,
                TickEnabled = false,
                Moveable = false,
                Model = Model.Library.Get("Models/skysphere.fbx"),
            };

            skySphere.SetMaterial(0, Material.Library.Get("Materials/cubemap.mat"));
            skySphere.Spawn();
        }
    }
}