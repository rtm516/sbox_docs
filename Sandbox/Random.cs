﻿namespace Sandbox
{
    public class Random
    {
        /// <summary>
        /// Generates a random number between the min and max values
        /// </summary>
        /// <param name="min">The minimum value to generate</param>
        /// <param name="max">The maximum value to generate</param>
        /// <returns>The number generated</returns>
        public static int Int(int min, int max) { return -1; }
    }
}
