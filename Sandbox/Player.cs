﻿namespace Sandbox
{
    public class Player
    {
        /// <summary>
        /// The local clients player object
        /// </summary>
        public static Player Local;

        /// <summary>
        /// The controllable object the current player is controlling
        /// </summary>
        public Controllable Controlling;

        /// <summary>
        /// The team index of the current player
        /// </summary>
        public int Team;

        /// <summary>
        /// The name of the current player
        /// </summary>
        public string Name;

        /// <summary>
        /// The camera that the player is looking at
        /// </summary>
        public CameraEntity ViewTarget;

        /// <summary>
        /// Adds a message to the chat box as the player
        /// </summary>
        /// <param name="message">Message to add to the chat box</param>
        public void Message(string message) { }
    }
}
