﻿namespace Sandbox
{
    public class BaseEntity
    {
        /// <summary>
        /// The position of the entity
        /// </summary>
        public Vector3 Position;

        /// <summary>
        /// Is the entity valid
        /// </summary>
        public bool IsValid;

        /// <summary>
        /// Whether to send this to the client?
        /// </summary>
        public bool Replicates;

        /// <summary>
        /// The player that owns the current entity
        /// </summary>
        public BaseEntity Owner;

        /// <summary>
        /// Called when the entity is spawned
        /// </summary>
        public void Spawn() { }

        /// <summary>
        /// Called when the entity is destroyed
        /// </summary>
        public void Destroy() { }

        /// <summary>
        /// Called when the entity is created
        /// </summary>
        protected virtual void Initialize() { }
    }
}
