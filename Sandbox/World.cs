﻿namespace Sandbox
{
    public class World
    {
        /// <summary>
        /// Creates an entity with the specified object and returns it
        /// </summary>
        /// <returns>
        /// An instance of the object passed in
        /// </returns>
        public static T CreateAndSpawnEntity<T>() { return default; }

        /// <summary>
        /// Plays a sound to the player
        /// </summary>
        /// <param name="soundPath">The sounds path</param>
        public static void PlaySound2D(string soundPath) { }
    }
}
