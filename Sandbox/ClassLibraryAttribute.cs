﻿using System;

namespace Sandbox
{
	internal class ClassLibraryAttribute : Attribute
	{
		public string Name;

		public ClassLibraryAttribute() { }

		public ClassLibraryAttribute(string Name)
		{
			this.Name = Name;
		}
	}
}