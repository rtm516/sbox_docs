﻿namespace Sandbox
{
    public class Chatbox
    {
        /// <summary>
        /// Adds a message to the chat box
        /// </summary>
        /// <param name="playerName">The players name</param>
        /// <param name="message">The message</param>
        /// <param name="textColor">The color of the message</param>
        public void AddMessage(string playerName, string message, Color textColor) { }
    }
}
