
namespace Sandbox
{
    [ClassLibrary]
    public class PhysicsConstraintEntity : BaseEntity
    {
        public PhysicsConstraintEntity() { }

        public bool AngularDriveModeSlerp;
        public bool IsBroken;
        public string ConstraintBone2;
        public string ConstraintBone1;
        public BaseWorldEntity ConstraintEntity2;
        public BaseWorldEntity ConstraintEntity1;
        public Action OnBroken;
        public bool OrientationDriveSlerp;
        public float CurrentTwist;
        public bool CollisionEnabled;
        public float CurrentSwing1;
        public float CurrentSwing2;
        public Vector3 AngularVelocityTarget;
        public Vector3 LinearVelocityTarget;
        public Vector3 LinearPositionTarget;
        public bool AngularVelocityDriveSlerp;

        public void Break() { }
        public void GetConstraintForce(out Vector3 linearForce, out Vector3 angularForce) { }
        public void SetAngularBreakable(bool enabled, double threshold = 0) { }
        public void SetAngularDriveParams(double positionStrength, double velocityStrength, double forceLimit) { }
        public void SetAngularSwing1Limit(ConstraintMotion motion, double angle) { }
        public void SetAngularSwing2Limit(ConstraintMotion motion, double angle) { }
        public void SetAngularTwistLimit(ConstraintMotion motion, double angle) { }
        public void SetAngularVelocityDriveTwistAndSwing(bool enableTwist, bool enableSwing) { }
        public void SetLinearBreakable(bool enabled, double threshold = 0) { }
        public void SetLinearDriveParams(double positionStrength, double velocityStrength, double forceLimit) { }
        public void SetLinearPositionDrive(bool enableX, bool enableY, bool enableZ) { }
        public void SetLinearVelocityDrive(bool enableX, bool enableY, bool enableZ) { }
        public void SetLinearXLimit(ConstraintMotion motion, double size) { }
        public void SetLinearYLimit(ConstraintMotion motion, double size) { }
        public void SetLinearZLimit(ConstraintMotion motion, double size) { }
        public void SetOrientationDriveTwistAndSwing(bool enableTwist, bool enableSwing) { }
        protected virtual void Broken() { }
        protected override void Initialize() { }
    }
}