﻿namespace Sandbox
{
    public class Time
    {
        /// <summary>
        /// The current time in seconds
        /// </summary>
        public static int Now;
    }
}
