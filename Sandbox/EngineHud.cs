﻿namespace Sandbox
{
    public class EngineHud
    {
        /// <summary>
        /// The chatbox linked with the current hud
        /// </summary>
        public Chatbox Chatbox;

        /// <summary>
        /// Sends a message to all clients
        /// </summary>
        /// <param name="message">The message to send</param>
        public void BroadcastMessage(string message) { }
    }
}
