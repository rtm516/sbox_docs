﻿using Sandbox;

namespace Examples.Other
{
    [ClassLibrary]
    public class Citizen : SkeletalMeshEntity
    {
        Tweener Tweener = new Tweener(0, 1, 2.0, Ease.Sine.PingPong);

        protected override void Initialize()
        {
            base.Initialize();

            Model = SkeletalModel.Library.Get("Models/Citizen/Citizen.fbx");

            var Citizen_Skin = Material.Library.Get("Models/Citizen/Citizen_Skin.mat");
            var Citizen_Eyes = Material.Library.Get("Models/Citizen/Citizen_Eyes.mat");
            var Citizen_Eyebrows = Material.Library.Get("Models/Citizen/Citizen_Eyebrows.mat");
            var Citizen_Teeth = Material.Library.Get("Models/Citizen/Citizen_Teeth.mat");

            SetMeshMaterial("BodyChest_Citizen_Skin", Citizen_Skin);
            SetMeshMaterial("BodyLegs_Citizen_Skin", Citizen_Skin);
            SetMeshMaterial("BodyFeet_Citizen_Skin", Citizen_Skin);
            SetMeshMaterial("BodyHands_Citizen_Skin", Citizen_Skin);
            SetMeshMaterial("BodyHead_Citizen_Skin", Citizen_Skin);
            SetMeshMaterial("BodyHead_Citizen_Eyes", Citizen_Eyes);
            SetMeshMaterial("BodyHead_Citizen_Eyebrows", Citizen_Eyebrows);
            SetMeshMaterial("BodyHead_Citizen_Teeth", Citizen_Teeth);

            ShowMeshSection("BodyChest_Citizen_Skin", false);
            ShowMeshSection("BodyLegs_Citizen_Skin", false);
            ShowMeshSection("BodyFeet_Citizen_Skin", false);
            ShowMeshSection("BodyHands_Citizen_Skin", true);
            ShowMeshSection("BodyHead_Citizen_Skin", true);
            ShowMeshSection("BodyHead_Citizen_Eyes", true);
            ShowMeshSection("BodyHead_Citizen_Eyebrows", true);
            ShowMeshSection("BodyHead_Citizen_Teeth", true);

            foreach (var animation in Model.IncludeAnimations)
            {
                AddAnimation(animation.Name, animation);
            }

            var animations = new string[]
            {
                "Citizen@CrouchIdle.fbx",
                "Citizen@CrouchWalk.fbx",
                "Citizen@CrouchWalkDown.fbx",
                "Citizen@CrouchWalkLeft.fbx",
                "Citizen@CrouchWalkRight.fbx",
                "Citizen@CrouchWalkUp.fbx",
                "Citizen@Hold.fbx",
                "Citizen@Idle.fbx",
                "Citizen@IdleJump.fbx",
                "Citizen@IdleLaugh.fbx",
                "Citizen@LookAt.fbx",
                "Citizen@Run.fbx",
                "Citizen@RunBack.fbx",
                "Citizen@RunJumpLand.fbx",
                "Citizen@Walk.fbx",
                "Citizen@WalkBack.fbx",
            };

            foreach (var animation in animations)
            {
                AddAnimation(animation, Animation.Library.Get($"Models/Citizen/Animations/{animation}", new AnimationLibrary.Settings
                {
                    Model = Model,
                    Looped = true,
                }));
            }

            PlayAnimation("Citizen@Idle.fbx");

            PhysicsAsset = PhysicsAsset.Library.Get("Models/Citizen/Citizen.phy");

            var trousers = new SkeletalMeshEntity
            {
                Owner = this,
                Model = SkeletalModel.Library.Get("Models/Clothes/Trousers/Trousers.FBX"),
            };

            trousers.SetMeshMaterial(0, Material.Library.Get("Models/Clothes/Trousers/Materials/SmartTrousers/SmartTrousers.mat"));
            trousers.Spawn();
            trousers.AttachTo(this, AttachmentRule.KeepRelative);
            trousers.MasterPose = this;

            var shirt = new SkeletalMeshEntity
            {
                Owner = this,
                Model = SkeletalModel.Library.Get("Models/Clothes/Shirt_LongSleeve/Shirt_LongSleeve.FBX"),
            };

            shirt.SetMeshMaterial(0, Material.Library.Get("Models/Clothes/Shirt_LongSleeve/Materials/PoliceUniformShirt/PoliceUniformShirt_Albedo.png"));
            shirt.Spawn();
            shirt.AttachTo(this, AttachmentRule.KeepRelative);
            shirt.MasterPose = this;

            var hat = new SkeletalMeshEntity
            {
                Owner = this,
                Model = SkeletalModel.Library.Get("Models/Clothes/Hat_Uniform/Hat_Uniform.FBX"),
            };

            hat.SetMeshMaterial(0, Material.Library.Get("Models/Clothes/Hat_Uniform/Materials/PoliceUniformHat/PoliceUniformHat_Albedo.png"));
            hat.Spawn();
            hat.AttachTo(this, AttachmentRule.KeepRelative);
            hat.MasterPose = this;

            var shoes = new SkeletalMeshEntity
            {
                Owner = this,
                Model = SkeletalModel.Library.Get("Models/Clothes/Shoes/Shoes.FBX"),
            };

            shoes.SetMeshMaterial(0, Material.Library.Get("Models/Clothes/Shoes/Materials/PoliceUniformShoes/PoliceUniformShoes_Albedo.png"));
            shoes.Spawn();
            shoes.AttachTo(this, AttachmentRule.KeepRelative);
            shoes.MasterPose = this;

            SimulatePhysics = true;
        }

        protected override void Tick()
        {
            base.Tick();

            Tweener.Update();
            var delta = Tweener.Value;
            delta = Math.Clamp(delta, 0.01, 1);

            SetMorphTarget("Mouth_SmileOpen", delta);
            SetMorphTarget("Mouth_SmileClosed", delta);
            SetMorphTarget("Mouth_Closed", 0);
            SetMorphTarget("Mouth_Ooo", 0.0);
            SetMorphTarget("Head_Ghost", 0);
            SetMorphTarget("EyebrowsR_Shocked", delta);
            SetMorphTarget("EyebrowsL_Shocked", delta);
        }
    }
}
