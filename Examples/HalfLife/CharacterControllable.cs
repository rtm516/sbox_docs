﻿using Sandbox;

namespace HalfLife
{
    public class CharacterControllable : Controllable
    {
        protected CapsuleEntity Collider;

        protected double Speed => 900.0;
        protected double AirSpeed => 200.0;
        protected double Acceleration => 10.0;
        protected double AirAcceleration => 70.0;
        protected double GroundFriction => 8.0;
        protected double AirFriction => 0.0;
        protected double Gravity => 2000.0;
        protected double JumpHeight => 80.0;
        protected double MaxSlope => 0.75;
        protected double EyeHeight => 40.0;

        protected Vector3 MovementVelocity = Vector3.Zero;
        protected Vector3 WishDirection = Vector3.Zero;
        protected bool WishJump = false;
        protected bool IsGrounded = false;
        protected Vector3 GroundNormal = Vector3.Up;

        protected double MaxSimulationTimeStep => 0.01;
        protected int MaxSimulationIterations => 8;
        protected static double MinTickTime => 1e-6;

        protected override void Initialize()
        {
            base.Initialize();

            Collider = new CapsuleEntity
            {
                Replicates = false,
                Owner = this,
                Hidden = true,
                CollisionProfileName = "Pawn",
            };

            Collider.AttachTo(this, AttachmentRule.KeepRelative);
            Collider.SetCapsuleSize(30, 70);
            Collider.Spawn();

            SweepEntity = Collider;
        }

        protected override void DoInput()
        {
            base.DoInput();

            var movementInput = Vector3.Zero;

            if (Input.IsDown(Button.W)) movementInput += Vector3.Forward;
            if (Input.IsDown(Button.S)) movementInput -= Vector3.Forward;
            if (Input.IsDown(Button.D)) movementInput += Vector3.Right;
            if (Input.IsDown(Button.A)) movementInput -= Vector3.Right;

            if (Input.IsDown(Button.LeftShift)) movementInput *= 10.0;
            else if (Input.IsDown(Button.LeftControl)) movementInput *= 0.5;

            double yaw = Input.Value(Axis.MouseX) * 0.05;
            double pitch = Input.Value(Axis.MouseY) * -0.05;

            AddMovementInput(movementInput);
            AddEyeInput(pitch, yaw);

            WishJump = Input.IsDown(Button.SpaceBar);
        }

        double GetSimulationTimeStep(double RemainingTime, int Iterations)
        {
            if (RemainingTime > MaxSimulationTimeStep)
            {
                if (Iterations < MaxSimulationIterations)
                {
                    RemainingTime = Math.Min(MaxSimulationTimeStep, RemainingTime * 0.5);
                }
            }

            return Math.Max(MinTickTime, RemainingTime);
        }

        protected override void TickMovement()
        {
            base.TickMovement();

            ViewLocationOffset = Vector3.Up * EyeHeight;

            var inputVector = ConsumeInputVector();
            var yawRotation = Quaternion.FromAngles(0, ControlRotation.Yaw, 0);
            WishDirection = yawRotation.Forward * inputVector.x + yawRotation.Right * inputVector.y;

            var iterations = 0;
            var deltaTime = Time.Delta;
            var remainingTime = deltaTime;

            while ((remainingTime >= MinTickTime) && (iterations < MaxSimulationIterations))
            {
                iterations++;
                var timeTick = GetSimulationTimeStep(remainingTime, iterations);
                remainingTime -= timeTick;

                PhysicsStep(timeTick);
            }
        }

        void PhysicsStep(double dt)
        {
            if (IsGrounded && WishJump)
            {
                MovementVelocity = MovementVelocity.WithZ(Math.Sqrt(2.0 * Gravity * JumpHeight));
                WishJump = false;
                IsGrounded = false;
            }

            ApplyAcceleration(WishDirection.Normal, IsGrounded ? Speed : AirSpeed, IsGrounded ? Acceleration : AirAcceleration, dt);
            ApplyFriction(IsGrounded ? GroundFriction : AirFriction, dt);

            if (!IsGrounded)
            {
                ApplyGravity(dt);
            }

            var oldPosition = Collider.Position;
            var movementHit = new HitResult(1.0);
            var movementDelta = MovementVelocity * dt;

            PositionCorrected = false;

            SafeMove(movementDelta, Quaternion.Identity, true, ref movementHit, TeleportType.None);
            SlideAlongSurface(movementDelta, 1.0 - movementHit.Fraction, movementHit.Normal, ref movementHit, true);

            if (!PositionCorrected)
            {
                MovementVelocity = (Collider.Position - oldPosition) / dt;
            }

            TraceGround();
        }

        protected void TraceGround()
        {
            IsGrounded = false;

            var hit = Sweep(out var result, Collider.Position, Collider.Position - (Vector3.Up * 5.0));

            if (result.Fraction == 1.0 ||
                MovementVelocity.z > 0 && Vector3.Dot(MovementVelocity, result.ImpactNormal) > 10 ||
                result.ImpactNormal.z < MaxSlope)
            {
                return;
            }

            IsGrounded = true;
            GroundNormal = result.Normal;
        }

        protected void ApplyGravity(double dt)
        {
            MovementVelocity += (Vector3.Up * (-Gravity * dt));
        }

        protected void ApplyFriction(double friction, double dt)
        {
            var speed = MovementVelocity.Length;
            var drop = speed * friction * dt;
            var newSpeed = speed - drop;

            if (newSpeed < 0.0)
            {
                newSpeed = 0.0;
            }

            if (speed > 0.0)
            {
                newSpeed /= speed;
            }

            MovementVelocity *= newSpeed;
        }

        protected void ApplyAcceleration(Vector3 wishDirection, double wishSpeed, double acceleration, double dt)
        {
            var currentSpeed = Vector3.Dot(wishDirection, MovementVelocity);
            var addSpeed = wishSpeed - currentSpeed;

            if (addSpeed <= 0.0)
            {
                return;
            }

            var accelerationSpeed = acceleration * dt * wishSpeed;

            if (accelerationSpeed > addSpeed)
            {
                accelerationSpeed = addSpeed;
            }

            MovementVelocity += accelerationSpeed * wishDirection;
        }
    }
}