using Sandbox;

namespace Shoutcast
{
    [ClassLibrary]
    public class RadioEntity : SoundEntity
    {
        private SoundStream _soundStream;
        private ShoutcastStream _shoutcastStream;

        private byte[] _buffer = new byte[44100 * 4];
        private bool _readingData = false;

        protected override void Initialize()
        {
            base.Initialize();

            if (Client)
            {
                _shoutcastStream = new ShoutcastStream();
                _shoutcastStream.ConnectAsync();

                _soundStream = new SoundStream(2, 44100);
                Sound = _soundStream;

                AdjustAttenuation(new SoundAttenuation
                {
                    Shape = AttenuationShape.Sphere,
                    ShapeExtents = new Vector3(1, 0, 0),
                    FalloffDistance = 5000
                });
            }
        }

        protected override void Destroyed()
        {
            base.Destroyed();

            _shoutcastStream?.Dispose();
        }

        protected override void Tick()
        {
            base.Tick();

            if (!_readingData)
            {
                ReadData();
            }
        }

        protected async void ReadData()
        {
            if (_readingData) return;

            _readingData = true;

            if (await _shoutcastStream.ReadDataAsync(_buffer))
            {
                _soundStream.QueueData(_buffer);
            }

            _readingData = false;
        }
    }
}