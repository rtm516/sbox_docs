using Sandbox;
using System;
using System.IO;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace Shoutcast
{
    public class ShoutcastStream : IDisposable
    {
        private TcpClient Socket;
        private NetworkStream SocketStream;
        private StreamWriter SocketWriter;

        private string Host => "23.29.71.154";
        private int Port => 8140;
        private string Directory => "stream";

        private bool SocketConnected = false;
        public bool StartStreaming { get; private set; }

        private MP3Sharp.MP3Stream MP3Stream;

        private string ReadLine()
        {
            if (!SocketConnected)
            {
                return "";
            }

            var line = string.Empty;

            while (SocketStream.DataAvailable)
            {
                byte[] b = new byte[1];
                SocketStream.Read(b, 0, 1);

                line += (char)b[0];

                if ((char)b[0] == '\n')
                {
                    break;
                }
            }

            return line;
        }

        public async Task ConnectAsync()
        {
            Socket = new TcpClient();

            if (!Socket.ConnectAsync(Host, Port).Wait(1000))
            {
                Log.Error($"Connection timed out: {Host}:{Port}");

                return;
            }

            SocketConnected = true;

            Log.Info($"Connected to: {Host}:{Port}");

            SocketStream = Socket.GetStream();
            SocketWriter = new StreamWriter(SocketStream);

            var getRequest = $"GET /{Directory} HTTP/1.0\nIcy-MetaData: {false}\n\r\n";

            SocketWriter.Write(getRequest);
            SocketWriter.Flush();

            while (SocketConnected)
            {
                var line = ReadLine();

                if (line.Length > 0)
                {
                    Log.Info(line);

                    if (line[0] == '\r' && line[1] == '\n')
                    {
                        StartStreaming = true;

                        break;
                    }
                }

                await Task.Yield();
            }

            MP3Stream = new MP3Sharp.MP3Stream(SocketStream);
        }

        public async Task<bool> ReadDataAsync(byte[] buffer)
        {
            if (MP3Stream == null) return false;

            var r = await MP3Stream.ReadAsync(buffer, 0, buffer.Length);

            return r == buffer.Length;
        }

        private void Disconnect()
        {
            if (SocketConnected)
            {
                SocketWriter.Close();
                Socket.Close();

                if (MP3Stream != null)
                {
                    MP3Stream.Close();
                }

                SocketConnected = false;
                StartStreaming = false;
            }
        }

        public void Dispose()
        {
            Disconnect();
        }
    }
}