﻿using Sandbox;

namespace SandboxGame.Entities
{
    [ClassLibrary]
    public class Balloon : BaseModelEntity, ICanBeDamaged, ICanBeRemoved
    {
        public double Force { get; set; }

        [Replicate(OnChange = nameof(OnTintChanged))]
        public Color Tint { get; set; }

        protected Material TintMaterial { get; set; }

        protected override void Initialize()
        {
            base.Initialize();

            if (Authority)
            {
                Mass = 1000.0;
                LinearDamping = 0.0;
                AngularDamping = 0.0;
                EnableGravity = false;
                Collision = true;
                SimulatePhysics = true;
            }

            TintMaterial = new Material { Shader = "Standard" };
            TintMaterial.Set("Color", Tint);
            TintMaterial.Set("Shininess", 1.0);
            TintMaterial.Set("Metallic", 0.0);
            TintMaterial.Set("Specular", 1.0);

            SetMaterial(0, TintMaterial);
        }

        protected override void Tick()
        {
            base.Tick();

            if (Authority)
            {
                AddForce(Vector3.Up * ((Force * 5000.0) * Time.Delta), true);
            }
        }

        public bool TakeDamage(DamageInfo damage)
        {
            if (Authority)
            {
                BroadcastPop();
                Destroy();
            }

            return true;
        }

        [Multicast]
        protected void BroadcastPop()
        {
            if (!Client) return;

            PlaySound("Sounds/balloon_pop_cute.wav", 1.0, Random.Double(1.0, 1.2));

            new Effects.BalloonPop
            {
                Position = Position,
                Tint = Tint,
            }
            .Spawn();
        }

        protected void OnTintChanged(Color oldValue, Color newValue)
        {
            if (TintMaterial != null)
            {
                TintMaterial.Set("Color", newValue);
            }
        }

        public bool Remove()
        {
            Destroy();

            return true;
        }
    }
}