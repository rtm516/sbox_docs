﻿using Sandbox;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace SandboxGame.Tools
{
    [DataContract]
    [ClassLibrary(Name = "tool_leafblower")]
    public class LeafBlower : BaseTool
    {
        public override string ShortName => "Leaf Blower";
        public override bool HasOptions => true;
        public override bool LeftClickAutomatic => true;

        [DataMember]
        [DisplayName("Force")]
        public double Force { get; set; } = 500.0;

        [DataMember]
        [DisplayName("Max Distance")]
        public double MaxDistance { get; set; } = 1000.0;

        protected override bool LeftClick(HitResult hitResult)
        {
            if (!Authority) return false;
            if (!IsEntityValid(hitResult.Entity)) return false;

            if (hitResult.Entity is BaseWorldEntity entity &&
                entity.IsSimulatingPhysics(hitResult.BoneName))
            {
                var distance = Vector3.DistanceBetween(hitResult.TraceStart, hitResult.Location);
                var ratio = Math.Clamp((1.0 - (distance / MaxDistance)), 0.0, 1.0);
                var force = hitResult.TraceDirection * (Force * ratio);
                var offset = hitResult.Location;

                entity.AddImpulseAtLocation(force * entity.Mass, offset);
            }

            return false;
        }
    }
}