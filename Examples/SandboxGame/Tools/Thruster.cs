﻿using Sandbox;
using System.Runtime.Serialization;

namespace SandboxGame.Tools
{
    [DataContract]
    [ClassLibrary(Name = "tool_thruster")]
    public class Thruster : BaseTool
    {
        public override string ShortName => "Thruster";
        public override bool HasOptions => true;
        public override bool LeftClickAutomatic => false;
        public override bool RightClickAutomatic => false;

        [DataMember]
        public double Force { get; set; } = 5000.0;

        protected override bool LeftClick(HitResult hitResult)
        {
            return SpawnThruster(hitResult);
        }

        protected bool SpawnThruster(HitResult hitResult)
        {
            if (hitResult.Entity is BaseWorldEntity hitEntity)
            {
                if (!Authority) return true;

                if (hitEntity is Entities.Thruster thruster)
                {
                    thruster.Force = Force;

                    return true;
                }

                bool attach = hitEntity.SimulatePhysics;
                var hitRotation = Quaternion.VectorToOrientation(hitResult.Normal) * Quaternion.FromAngles(-90, 0, 0);
                var hitTransform = new Transform(hitResult.Location, hitRotation, Vector3.Zero);
                var relativeTransform = hitTransform;

                if (attach)
                {
                    relativeTransform = relativeTransform.GetRelativeTransform(hitEntity.Transform);
                }

                thruster = new Entities.Thruster
                {
                    Model = Model.Library.Get("Models/ThrusterProjector.fbx"),
                    SimulatePhysics = !attach,
                    RelativePosition = relativeTransform.Translation,
                    RelativeRotation = relativeTransform.Rotation,
                    Force = Force,
                    Owner = hitEntity,
                    AbsoluteScale = true,
                    Collision = true,
                };

                thruster.SetMaterial(0, Material.Library.Get("Models/ThrusterProjector.tga"));

                if (attach)
                {
                    thruster.AttachTo(hitEntity, AttachmentRule.KeepRelative, true);
                }

                thruster.Spawn();

                return true;
            }

            return false;
        }
    }
}