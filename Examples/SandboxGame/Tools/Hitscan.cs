﻿using Sandbox;
using System.Runtime.Serialization;

namespace SandboxGame.Tools
{
    [DataContract]
    [ClassLibrary(Name = "tool_hitscan")]
    public class Hitscan : BaseTool
    {
        public override string ShortName => "Hitscan";
        public override bool HasOptions => false;
        public override bool LeftClickAutomatic => false;
        public override bool RightClickAutomatic => false;

        private double Force => 1000.0;
        private double Distance => 100000.0;
        private double Damage => 10.0;

        protected override bool LeftClick(HitResult hitResult)
        {
            return Fire(hitResult, PickingRayStart, PickingRayDirection * Force, Damage, Distance);
        }

        public bool Fire(HitResult hitResult, Vector3 origin, Vector3 force, double damage, double distance)
        {
            if (hitResult.Entity == null || !hitResult.Entity.IsValid)
            {
                return false;
            }

            if (!ControlledByLocalPlayer)
            {
                return true;
            }

            foreach (var entity in hitResult.Entity.AncestorsAndSelf)
            {
                if (entity.Authority)
                {
                    Hit(entity, hitResult.Location, force, damage, hitResult.BoneIndex, hitResult.BoneName);
                }
                else
                {
                    ServerHit(entity, hitResult.Location, force, damage, hitResult.BoneIndex, hitResult.BoneName);
                }
            }

            return true;
        }

        protected void Hit(BaseEntity entity, Vector3 position, Vector3 force, double damage, int boneIndex, string boneName)
        {
            if (entity == null || !entity.IsValid) return;

            if (entity is ICanBeDamaged damageable)
            {
                damageable.TakeDamage(new DamageInfo
                {
                    Amount = damage,
                    Weapon = this,
                    Player = Owner,
                    Location = position,
                    Force = force,
                    Bone = boneIndex,
                    Time = Time.Now,
                });
            }

            if (entity is BaseWorldEntity worldEntity &&
                worldEntity.IsSimulatingPhysics(boneName))
            {
                worldEntity.AddImpulseAtLocation(force * worldEntity.Mass, position, boneIndex);
            }
        }

        [Server]
        protected void ServerHit(BaseEntity entity, Vector3 position, Vector3 force, double damage, int boneIndex, string boneName)
        {
            Hit(entity, position, force, damage, boneIndex, boneName);
        }
    }
}