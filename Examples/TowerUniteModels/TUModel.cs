﻿using System;
using System.IO;

namespace TowerUniteModels
{
	class TUModel
	{
		public void ParseV1(string filename)
		{
            using (var reader = new BinaryReader(FileSystem.OpenRead(filename)))
            {
                var magic = reader.ReadInt32();
                if (magic != 1279545165) return;

                var version = reader.ReadInt32();
                if (version != 1) return;

                var crc32 = reader.ReadInt32();

                // Bench.model has 67 unknown bytes after crc32
                reader.BaseStream.Seek(67, SeekOrigin.Current);

                var indexCount = reader.ReadInt32();
                Log.Info($"{indexCount} indices");

                // Bench.model has 20 unknown bytes after index count
                reader.BaseStream.Seek(20, SeekOrigin.Current);

                var indices = new uint[indexCount];
                uint maxIndex = 0;
                for (var i = 0; i < indexCount; ++i)
                {
                    var index = reader.ReadUInt32();
                    indices[i] = index;
                    maxIndex = Math.Max(index, maxIndex); // Vertex count from max index
                }

                // Bench.model has 8 unknown bytes after index array
                reader.BaseStream.Seek(8, SeekOrigin.Current);

                for (var i = 0; i <= maxIndex; ++i)
                {
                    var p = reader.ReadVector3();

                    var tangents = reader.ReadBytes(4);
                    var normals = reader.ReadBytes(4);

                    var tangent = new Vector3(tangents[0], tangents[1], tangents[2]) / 255.0;
                    tangent = (tangent * 2.0 - 1.0).Normal;

                    var normal = new Vector3(normals[0], normals[1], normals[2]) / 255.0;
                    normal = (normal * 2.0 - 1.0).Normal;

                    var uv = reader.ReadVector2();

                    mesh.Vertex.Add(new Vertex
                    {
                        Position = p,
                        Normal = normal,
                        Tangent = tangent,
                        UV1 = uv,
                        Color = Color.White,
                    });

                    Log.Info($"{i} - {p} uv = {uv} normal {normal}");
                }

                mesh.Indices.AddRange(indices);
            }
        }

        public void ParseV2()
        {
            using (var reader = new BinaryReader(FileSystem.OpenRead(filename)))
            {
                var magic = reader.ReadInt32();
                if (magic != 1279545165) return; // MODL

                var version = reader.ReadInt32();
                if (version != 1) return;

                var crc32 = reader.ReadInt32();

                // Bench.model has 67 unknown bytes after crc32
                reader.BaseStream.Seek(67, SeekOrigin.Current);

                var indexCount = reader.ReadInt32();
                Log.Info($"{indexCount} indices");

                var materialCount = reader.ReadInt32();
                Log.Info($"{materialCount} materials");

                // 6 bytes per material?
                reader.BaseStream.Seek(6 * materialCount, SeekOrigin.Current);

                // Bench.model has 14 unknown bytes after index count and material count
                reader.BaseStream.Seek(14, SeekOrigin.Current);

                var streamPosition = reader.BaseStream.Position;
                var indexArrayCount = reader.Read7BitEncodedIntOverride();
                Log.Info($"indexArrayCount {indexArrayCount} ({reader.BaseStream.Position - streamPosition} bytes)");

                var indices = new uint[indexCount];
                for (var i = 0; i < indexCount; ++i)
                {
                    var index = reader.ReadUInt32();
                    indices[i] = index;
                }

                mesh.Indices.AddRange(indices);

                // Maybe 2 bools for something
                var unknown0 = reader.ReadBoolean();
                var unknown1 = reader.ReadBoolean();

                var vertexSize = reader.ReadInt32();

                streamPosition = reader.BaseStream.Position;
                var vertexArrayCount = reader.Read7BitEncodedIntOverride() / vertexSize;
                Log.Info($"vertexArrayCount {vertexArrayCount}, vertex size {vertexSize}, ({reader.BaseStream.Position - streamPosition} bytes)");

                for (var i = 0; i < vertexArrayCount; ++i)
                {
                    var p = reader.ReadVector3();

                    var tangents = reader.ReadBytes(4);
                    var normals = reader.ReadBytes(4);

                    var tangent = new Vector3(tangents[0], tangents[1], tangents[2]) / 255.0;
                    tangent = (tangent * 2.0 - 1.0).Normal;

                    var normal = new Vector3(normals[0], normals[1], normals[2]) / 255.0;
                    normal = (normal * 2.0 - 1.0).Normal;

                    var uv = reader.ReadVector2();

                    mesh.Vertex.Add(new Vertex
                    {
                        Position = p,
                        Normal = normal,
                        Tangent = tangent,
                        UV1 = uv,
                        Color = Color.White,
                    });
                }
            }
        }
	}
}
